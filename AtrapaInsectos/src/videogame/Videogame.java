/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** Creación del main videogame  
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Videogame extends Application {
    protected static  Scene scene ;
    protected static PanelControl panelcontrol ;
    public void start(Stage primaryStage) {
       panelcontrol = new PanelControl();
       
        
        scene = new Scene(panelcontrol.getRoot(), 500, 500);
        
        primaryStage.setTitle("Proyecto Poo");
        primaryStage.setScene(scene);
        primaryStage.show();
 
    }

    /**
     * @param args se inicializan los arumentos 
     */
    
    
    public static void main(String[] args) {
        launch(args);
    }

    /**Mientras sea falso ue está muerto el personaje se lanza el juego 
     * @param vacio sse detiene si se muere el personaje  
     */
    @Override
    public void stop() throws Exception {
         GamePane.morir = false;
         GamePane2.morir = false;
    }
    
}
