/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.util.Random;
import javafx.scene.image.Image;
import recursos.Constantes;

/** Creación del enemigo Lagartija 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Lagartija extends Insecto{
    private boolean atrapada= false;
    private int posi;
    private int force;
    public Lagartija(int force) {
        super("/lizard.png", 90,90);
        this.force = force;
        ubicar();
        //Random rnd = new Random()
        posicion = 1;//rnd.nextInt(2)
        
    }
    /** Se setea el cambio de imagen cuando sea atrapada la lagartija  
     * @param vacio  
     * @version 23/09/19
     */
    public void cambiarimage(){
        Image imagen = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/tela.png"),90,90,true,true);
        enemigos.setImage(imagen);
    }
    
    /** Se setea el regreso  de imagen cuando sea liberada la lagartija  
     * @param vacio  
     * @version 23/09/19
     */
    
    public void regresarImagen(){
        enemigos.setImage(img);
    }
    
    /** Se ubica la lagartija en el inicio en una posicion aleatoria  
     * @param vacio  
     * @version 23/09/19
     */
    public void ubicar(){
        Random rnd = new Random();
       
        posi = rnd.nextInt(4);  
        double y =0;
        double x =0;
          if (posi == 0){
              x = 25;
              y = 25;
                      
          }
   
        
         if (posi == 1){
             x = Constantes.ANCHO-72;
             y = 25;    
            
        }
    
         if (posi == 2){
            x = Constantes.ANCHO-72;
            y = Constantes.ALTO-72;  
        }
         if (posi == 3){
            x = 25;
            y = Constantes.ALTO-72;  
            }
         getEnemigos().setLayoutX(x);
         getEnemigos().setLayoutY(y);
    }
    
    /** Se setea el camino de la lagartija y se aegura que no esté atrapada 
     * @param vacio  
     * @version 23/09/19
     */
    @Override
    public void mover(){
        
        if (posicion == 1 && !atrapada){
            double x;
            
            if (xtop){
                x=enemigos.getLayoutX()+Constantes.DESPLAZAMIENTO; 
            }else{
                x=enemigos.getLayoutX()-Constantes.DESPLAZAMIENTO;
            }
            
            double y = (210-210*Math.sin(force*x*(Math.PI/180)));
           
                if (x>=0 && x<= Constantes.ANCHO-50 && y>=0 && y<= Constantes.ALTO-50  )   {           
                  enemigos.setLayoutY(y);
                  enemigos.setLayoutX(x); 
            }
        }
        if (posicion == 0){
            double y;
            
            if (ytop){
                y=enemigos.getLayoutX()+Constantes.DESPLAZAMIENTO; 
            }else{
                y=enemigos.getLayoutX()-Constantes.DESPLAZAMIENTO;
            }
            double x = (240-230*Math.sin(force*y*(Math.PI/180)));
            System.out.println(x+"  0   "+y);
                if (x>=0 && x<= Constantes.ANCHO-50 && y>=0 && y<= Constantes.ALTO-50  )   {           
                  enemigos.setLayoutY(x);
                  enemigos.setLayoutX(y); 
            }
        }
    }
    
    /** Boleano para asegurarse siestá atrapada  
     * @param vacio  
     * @version 23/09/19
     */
    public boolean isAtrapada() {
        return atrapada;
    }
    

    public void setAtrapada(boolean atrapada) {
        this.atrapada = atrapada;
    }
    
    @Override
    public void consultar(){
        if (enemigos.getLayoutY()<=10){
            ytop = false;
        }
        if (enemigos.getLayoutY()>=Constantes.ALTO-70){
            ytop=true;
        }
        if (enemigos.getLayoutX()<=10){
            xtop = true;
        }
        if (enemigos.getLayoutX()>=Constantes.ANCHO-70){
            xtop=false;
        }
        
        
    }
}
