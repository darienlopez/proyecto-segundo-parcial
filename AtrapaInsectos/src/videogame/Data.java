/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;




/** Creación de la informacion de las partidas 
 *
 * @author Jsbarber
 * @version 23/09/19
 */

public class Data {
    protected static Path path = Paths.get("src/recursos/partidas.csv");
    
     /** Creación leerInfromacion para lectura de los datos 
     * @param vacio lee la informacion en el archivo 
     * @version 23/09/19
     */
    public static void leerInformacion() throws IOException{
        
        try(BufferedReader br = new BufferedReader(new FileReader(path.toFile()))){
            String linea = null;
            while((linea = br.readLine())!=null){ 
                //Date fecha=null
                String [] campo = linea.split(",");        
                
                PanelControl.partidas.add(new Partida(campo[0],campo[1],Integer.parseInt(campo[2]),campo[3]));
                    
                }
            }
        }
        
     /** Escribe la informacion de la partida en el achivo 
     * @param String escribe cada linea que recive en el csv
     * @version 23/09/19
     */
    
    public static void escribirInformacion(String s) throws  IOException{
        try ( BufferedWriter wr = new BufferedWriter(new FileWriter(path.toFile(),true))){
            wr.write(s);
            wr.newLine();           
        }
        
    }
    
    private Data() {
    throw new IllegalStateException("");
  }
}
