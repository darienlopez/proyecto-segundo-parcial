/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.util.Random;
import recursos.Constantes;

/** Creación del panel del Primer Nivel 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Mosca extends Insecto{
    
    public Mosca(){
        super("/anty.png",40,40);
        
    }
    /** Se setea el movimiento de la mosca 
     * @param vacio se setea el camino de las moscas  
     * @version 23/09/19
     */
    @Override
    public void mover(){
        Random rnd = new Random();
        posicion = rnd.nextInt(2);  

        if (posicion == 0){
            double y;
            if (ytop){
                y=enemigos.getLayoutY()-Constantes.DESPLAZAMIENTO; 
            }else{
                y=enemigos.getLayoutY()+Constantes.DESPLAZAMIENTO;
            }
            
            if (y>=0 && y<= Constantes.ALTO-50 )   { 
                
                enemigos.setLayoutY(y);         
            
        }
          }
  
         if (posicion == 1){
            double x;
            if (ytop){
                x=enemigos.getLayoutX()-Constantes.DESPLAZAMIENTO; 
            }else{
                x=enemigos.getLayoutX()+Constantes.DESPLAZAMIENTO;
            }
            if (x>0 && x< Constantes.ANCHO-50 )   { 
              
            enemigos.setLayoutX(x);        
            }
    }
    }
    
}
