/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.util.Random;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import recursos.Constantes;

/** Creación de la clase Piedra
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Piedra {
    private Image img;
    private ImageView piedraimg;
    private double x1;
    private double y1;
    public Piedra(){  
    img = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/roca.png")
        ,40,40,true,true);
    piedraimg = new ImageView(img);
    crear();
    }
    
        /** Creación de la piedra y sus dimensiones 
     * @param vacio 
     * @version 23/09/19
     */
    public void crear(){
        Random x = new Random();
            x1 = 50 + (Constantes.ANCHO-50 - 50) * x.nextDouble();
           Random  y = new Random();
         y1 = 50 + (Constantes.ALTO-50 - 50) * y.nextDouble();
          piedraimg.setLayoutX(x1);
          piedraimg.setLayoutY(y1);
          
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public Node getPiedra() {
        return piedraimg;
    }

    public void setPiedra(ImageView piedra) {
        this.piedraimg = piedra;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }
    
    
}
