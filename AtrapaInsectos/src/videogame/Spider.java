/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import recursos.Constantes;

/** Creación del jugador araña 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Spider {
    protected static ImageView spyder;
    protected static Image img;
    protected  static int xvel;
    protected static int yvel;
    protected static boolean superpoder;
    
    /** Constructor del personaje 
     * @param vacio crea el personaje y setea la imagen de los recursos  
     * @version 23/09/19
     */
    public Spider(){
        
        // IMPORTANTE el constructor debe modificarse para poder crear los dos tipos de jugadores ejmplo que reciba un numero
        // y  en un condicional se asigne la imagen, recordar que tambien los personajes tendran moviemineto      
        img = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/spyder.png"),70,70,true,true);
        spyder = new ImageView(img); 
    }
    /** Nodo que recupera el objeto 
     * @param vacio q
     * @return spyder
     * @version 23/09/19
     */
     public Node getObjeto(){
        return spyder;
        
    }
     /** Se fija la posicion del personaje  
     * @param vacio se obtiene las cordenadas del personaje   
     * @version 23/09/19
     */
    public void fijarPosicion(double x, double y){
        spyder.setLayoutX(x);
        spyder.setLayoutY(y);
        
    }
     
     /** Describe el movimiento del personaje 
     * @param vacio Se fija la cantidad de pixeles que se mueve el personaje hacia arriba    
     * @version 23/09/19
     */
    public void up(){
        double y = getObjeto().getLayoutY() - 25;
        
        if (y>=0 && y<= Constantes.ALTO-50 )   { 
            yvel = 25;
            xvel = 0;
            this.getObjeto().setLayoutY(y);
            
        }
                    
            
    }
    /** Describe el movimiento del personaje 
     * @param vacio Se fija la cantidad de pixeles que se mueve el personaje hacia abajo    
     * @version 23/09/19
     */
    
    public void down(){
        double y = getObjeto().getLayoutY() + 25;
        
        if (y>=0 && y<= Constantes.ALTO-50 ){
            yvel = -25;
            xvel = 0;
            this.getObjeto().setLayoutY(y);
            
            
        }      
            
               
    }
    /** Describe el movimiento del personaje 
     * @param vacio Se fija la cantidad de pixeles que se mueve el personaje hacia derecha     
     * @version 23/09/19
     */
    
    public void right(){
        double x = this.getObjeto().getLayoutX() + 30;
        if (x>=0 && x<= Constantes.ANCHO-50){
            this.getObjeto().setLayoutX(x);  
            xvel = 25;
            yvel =0;
        }
          
    }
    /** Describe el movimiento del personaje 
     * @param vacio Se fija la cantidad de pixeles que se mueve el personaje hacia izquierda    
     * @version 23/09/19
     */
    
    public void left(){
        double x = this.getObjeto().getLayoutX() - 25;
        if (x>=0 && x<= Constantes.ANCHO-50 ){
            xvel=-25;
            yvel =0;
            this.getObjeto().setLayoutX(x);
              
        }
    }
    
}
