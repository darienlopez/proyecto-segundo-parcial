/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Creación del panel Partida
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Partida {
    private String nombre;
    private String fecha;
    private int puntos;
    private String nivel;

      /** Creación del panel del pscoreboard 
     * @param String,String,int,String para guardar los datos de partida  
     * @version 23/09/19
     */
    public Partida(String nombre, String fecha, int puntos, String nivel) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.puntos = puntos;
        this.nivel = nivel;
    }
    
      /** Creación del panel del primer nivel 
     * @param String,int,String para guardar los datos de partida  
     * @version 23/09/19
     */
    public Partida(String nombre, int puntos, String nivel) {
        this.nombre = nombre;
        this.fecha =  convertifecha();
        this.puntos = puntos;
        this.nivel = nivel;
    }
         /** Conversión para el formato fecha 
     * @param vacío 
     * @version 23/09/19
     */
    public String convertifecha(){
        Date fecha = new Date();
        SimpleDateFormat f1 =  new SimpleDateFormat("dd/MM/yyyy");
        return f1.format(fecha);
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

   

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    @Override
    public String toString() {
        return nombre + "," + fecha + "," + puntos + "," + nivel;
    }
    public void escribir(){
        try {
            Data.escribirInformacion(toString());
        } catch (IOException ex) {
            Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
