/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import recursos.Constantes;
import static videogame.Videogame.panelcontrol;
import static videogame.Videogame.scene;

/** Creación del panel del Primer Nivel 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class GamePane {
    private VBox mainroot;
    private Pane gameroot;
    private HBox panelinfo;
    private Spider spider;
    private Lagartija lagartija;
    private ImageView salir;
    private ImageView poder;
    private HBox vidas;
    
    protected static boolean morir=true;
    private int contadorVidas = 3;
    private Label tiempo;
    private Label puntos;
    private int contadorPuntos=0;
    private int contadorA;
    private int reloj;
    private ArrayList<Insecto> insectos;
    private boolean disminuir=true;
    private String nombre;
    private static final String MENSAJE = "Algo inesperado ocurrió";
    private static final String ACEPTAR1 = "ACEPTAR";
    private boolean boolpoder;
    
    Image tela= new Image(getClass().getResourceAsStream(Constantes.RUTA+"/tela.png"),40,40,true,true);
    public GamePane(String nombre){
        this.nombre = nombre;
        crearPanel();
        crearspider();
        moverPersonajes();
        crearMoscas();
        crearHormigas();
        Thread tiempo = new Thread(new Tiempo());
        tiempo.start();
    }
    
    /** Creación del panel del primer nivel 
     * @param vacio que agrega los paneles y los botones del menu  
     * @version 23/09/19
     */
    public void crearPanel(){
        mainroot = new VBox();
        gameroot = new Pane();
        insectos = new ArrayList<>();
        mainroot.setStyle("-fx-background-image: url('"+Constantes.RUTA+"/fondo_juego.jpg');"
               + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+Constantes.ANCHO+" "+(Constantes.ALTO)+"; "
                + "-fx-background-position: center center;");
        panelinfo = new HBox();
        Image exit = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/exit.png"),50,50,true,true);
        salir = new ImageView(exit);
        salir.setOnMouseClicked(new EventHandler<MouseEvent> (){
            
     /** Creación de clase anonima handle  
     * @param MouseEvent maneja el evento al hacer click en los botones aceptar o cancelar  
     * @version 23/09/19
     */
            public void handle(MouseEvent event) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText(null);
                alert.setTitle("Menu Pausa");
                alert.setContentText("Seleccione la opcion que desee");
                ButtonType salir = new ButtonType(ACEPTAR1);
                ButtonType reanudar = new ButtonType("CANCELAR");
                alert.getButtonTypes().clear(); 
                alert.getButtonTypes().addAll(salir,reanudar);
 
                Optional<ButtonType> option= alert.showAndWait();
            
         if (option.get() == salir) {
            scene.setRoot(panelcontrol.getRoot());
          } 
         else if (option.get() == null){
             System.err.println("No selecciono nada");
         }
            } 


            
        });
        
        vidas = new HBox();
        Font fuente = Font.font("veranda", FontWeight.BOLD,FontPosture.REGULAR, 22);
        puntos = new Label("0 Pts");
        tiempo = new Label("00:00");
        puntos.setFont(fuente);
        tiempo.setFont(fuente);
        puntos.setTextFill(Color.WHITE);
        tiempo.setTextFill(Color.WHITE);
        panelinfo.setSpacing(50);
        panelinfo.getChildren().addAll(tiempo,vidas,puntos,salir);
        
        
        
        panelinfo.setStyle("-fx-background-color: black;\n -fx-pading: 20; ");
        setearvidass();
        
        mainroot.getChildren().addAll(panelinfo,gameroot);
    }    
    
     /** Se setean las vidas al inicio del juego y durante el juego
     * @param vacio  maneja el sonido al perder las vidas y quitarlas si colisiona con la largartija 
     * @version 23/09/19
     */
    public void setearvidass(){
        vidas.getChildren().removeAll(vidas.getChildren());
        //System.out.println(contador_vidas)
        if(contadorVidas ==0){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText(null);
                alert.setTitle("Fin del juego");
                alert.setContentText("Se quedo sin vidas");
                ButtonType salir = new ButtonType(ACEPTAR1);
                
                Media media = new Media(new File("src/recursos/youlose.mp3").toURI().toString());
                MediaPlayer player = new MediaPlayer(media);
                MediaView mv = new MediaView(player);
                player.setAutoPlay(true);
                player.setVolume(1);
        
                player.play();
                gameroot.getChildren().add(mv);
             
               
                alert.getButtonTypes().clear(); 
                alert.getButtonTypes().addAll(salir);
 
                Optional<ButtonType> option= alert.showAndWait();
            
                if (option.get() == salir) {
                   Platform.exit();
                } 
                if(option.get() == null){
                    System.out.println();
                }

        }
        else
        {
        Image imagen = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/heart.png"),40,40,true,true);
        Image imagen2 = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/trans.png"),40,40,true,true);
        for(int i =0; i< contadorVidas; i++){
                vidas.getChildren().add(new ImageView(imagen));
            }
        
        for(int i =0; i< Math.abs(contadorVidas-3); i++){
                vidas.getChildren().add(new ImageView(imagen2));
               
            }
         
        }
    }
    
     /** isCollision revisar si se chocan los objetos 
     * @param Node  compara los nodos que recibe
     * @return  True or false si es que se están chocando los objetos 
     * @version 23/09/19
     */
    public static boolean isCollision(Node n1, Node n2){
        // todos los elementos graficos existen en la clase nodo
        Bounds b1 = n1.getBoundsInParent();
        Bounds b2 = n2.getBoundsInParent();
        if (b1.intersects(b2)) {
            return true;      
        }else{
            return false;
        }
    }
    
     /** Creación de evetno al tener colision  
     * @param vacio agrega al marcador los puntos si la araña choca con los insectos 
     * @version 23/09/19
     */
    public void chequearColisiones(){
             
        
        ArrayList<Insecto> insectos2 = new ArrayList<>();
        for(Insecto m: insectos) {
           
            if(isCollision(spider.getObjeto(), m.getEnemigos())){
                if (m instanceof Mosca){
                     contadorPuntos+=15;
                     
                }
                else if (m instanceof Hormiga ){
                   contadorPuntos+=10;
                }
                gameroot.getChildren().remove(m.getEnemigos());
                insectos2.add(m);
                
                Media media = new Media(new File("src/recursos/comer.mp3").toURI().toString());
                MediaPlayer player = new MediaPlayer(media);
                MediaView mv = new MediaView(player);
                player.setAutoPlay(true);
                player.setVolume(1);
        
                player.play();
                gameroot.getChildren().add(mv);
                
                m.setHilo(false);
                puntos.setText(String.valueOf(contadorPuntos) + " ptos");
            
                
            }
            
        }
        for (Insecto inse : insectos2 ){
           insectos.remove(inse);
            
        }
    }
    
     /** cambiarnivel es el método que se usa al pasar el nivel 1 
     * @param vacio Si el usuario decide seguir se le pregunta si desea continuar con el nivel 2 
     * @version 23/09/19
     */
    public void cambiarnivel(){
        if(insectos.size() == 0){
            morir = false;
        lagartija.setAtrapada(true);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText(null);
                alert.setTitle("Fin del Nivel 1");
                alert.setContentText("Nivel 1 Superado, Deseas continuar al siguiente nivel?");
                ButtonType aceptar = new ButtonType(ACEPTAR1);
               ButtonType salir = new ButtonType("Salir");
                alert.getButtonTypes().clear(); 
                alert.getButtonTypes().addAll(aceptar,salir);
 
                Optional<ButtonType> option= alert.showAndWait();
                if (option.get() == aceptar) {
                   TextInputDialog dialog = new TextInputDialog("");
                    
                    dialog.setContentText("Numero de mosca");     
                    Optional<String> result = dialog.showAndWait();
                    
                    TextInputDialog dialog2 = new TextInputDialog("");
                    
                    dialog2.setContentText("Numero de hormigas");     
                    Optional<String> result1 = dialog2.showAndWait();

          
                    GamePane2 nivel2 = new GamePane2(result.get(),result1.get(),nombre);
                scene.setRoot(nivel2.getMainroot());
                 } 
                else if (option.get() == salir) {
                    Partida partida = new Partida(nombre,contadorPuntos,"1");
                    partida.escribir();
                    Platform.exit();
                } 
                else{
                    System.out.println("");
                }
        }
        
    }
    
     /** Se verifica si se choca la araña con la lagartija 
     * @param vacio que disminuye una vida si se chocan 
     * @version 23/09/19
     */
     public void chequearColisioneslagartija(){
        
         if(isCollision(spider.getObjeto(),lagartija.getEnemigos())&& disminuir){
             contadorVidas -=1; 
             Thread tiempo = new Thread(new Tiempovidas(2));
             Media media = new Media(new File("src/recursos/lostlife.mp3").toURI().toString());
                MediaPlayer player = new MediaPlayer(media);
                MediaView mv = new MediaView(player);
                player.setAutoPlay(true);
                player.setVolume(1);
        
                player.play();
                gameroot.getChildren().add(mv);
             tiempo.start();
         }
    }
     
      /** Uso del poder 
     * @param vacio que agrega la tela en la posicion de la lagartija  
     * @version 23/09/19
     */
      public void chequearColisionespoder(){
         if (reloj>15 && reloj<=20){
            
            if(isCollision(poder,spider.getObjeto())){
                boolpoder = false; 
                lagartija.cambiarimage();
                lagartija.setAtrapada(true);
                gameroot.getChildren().remove(poder);
                Thread zizi = new Thread(new Lagartij());
                zizi.start();
            }
         }
    }
       /** Creación de moscas 
     * @param vacio que setea el número de moscas en el primer nivel   
     * @version 23/09/19
     */
    public void crearMoscas(){
        for (int i =0 ;i<4;i++){
            Mosca mosca = new Mosca();
            insectos.add(mosca);
            Thread enemigo = new Thread(new Enemigos(mosca));
            enemigo.start();
            gameroot.getChildren().add(mosca.getEnemigos());
        }
    }
     /** Creación de Hormigas 
     * @param vacio que setea el número de Hormigas en el primer nivel   
     * @version 23/09/19
     */
    
    public void crearHormigas(){
         for (int i =0 ;i<5;i++){
            Hormiga hormiga = new Hormiga();          
            insectos.add(hormiga);
            Thread enemigo = new Thread(new Enemigos(hormiga));
            enemigo.start();
            
            
            gameroot.getChildren().add(hormiga.getEnemigos());
        }
    }
     /** Creación la araña 
     * @param vacio que setea la araña en el primer nivel   
     * @version 23/09/19
     */
    public void crearspider(){
        spider = new Spider();
        lagartija = new Lagartija(2);
        Thread enemigo = new Thread(new Enemigos(lagartija));
        enemigo.start();    
        gameroot.getChildren().addAll(spider.getObjeto(),lagartija.getEnemigos());
        spider.fijarPosicion(200,200);
        
    }
    
     /** Se mueven los personajes 
     * @param vacio Se mueven los personajes en la pantalla mediante telcas    
     * @version 23/09/19
     */
    public void moverPersonajes(){
        
        //IMPORTANTE: UN OBJETO DE TIPO PANe
        gameroot.setFocusTraversable(true); 
        gameroot.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent event) {
                setearvidass();
                chequearColisiones();
                chequearColisioneslagartija();
                cambiarnivel();
                chequearColisionespoder();
                switch (event.getCode()) {
                    case UP: 
                        
                       spider.up();
                        break;
                    case DOWN: 
                        spider.down();
                       
                        break;  
                    case LEFT: 
                        spider.left();
                      
                        break;
                    case RIGHT: 
                        spider.right();
                        
                    default:     
                        break;                   
                                  
                }
                
                
                
            }
        });    
    }
    
    /** Se agrega la tela al gameroot
     * @param vacio que posiciona la tela en algun lugar del juego     
     * @version 23/09/19
     */
    public void crearPoder(){
       poder= new ImageView( new Image(getClass().getResourceAsStream(Constantes.RUTA+"/tela.png"),45,45,true,true));
       Random x = new Random();
       double rand = x.nextDouble();
       int x1 = (int) (50 + (Constantes.ANCHO-50 - 50) * rand);
       Random y = new Random();
       double rand2 = y.nextDouble();
       int y1 = (int) (50 + (Constantes.ALTO-50 - 50) * rand2);
       poder.setLayoutX(x1);
       poder.setLayoutY(y1);
        System.out.println(x1+" "+y1);
       gameroot.getChildren().add(poder);
       Thread power = new Thread(new Poder(poder));
       power.start();
         
         
    }
    
    /** Poder  
     * @param vacio crea el poder a los 15 segundos de juego     
     * @version 23/09/19
     */
    public void preguntarPoder(){
        
        if(reloj ==15){
           
           crearPoder();
        }
    }
    
    /** getMainroot 
     * @param vacio  
     * @return mainroot
     * @version 23/09/19
     */
    public VBox getMainroot() {
        return mainroot;
    }
      /** setMainroot
     * @param VBox  
     * @version 23/09/19
     */

    public void setMainroot(VBox mainroot) {
        this.mainroot = mainroot;
    }
   /** getGameroot
     * @param vacio
     * @return gameroot
     * @version 23/09/19
     */
    public Pane getGameroot() {
        return gameroot;
    }
    
       /** setGameroot
     * @param Pane 
     * @version 23/09/19
     */

    public void setGameroot(Pane gameroot) {
        this.gameroot = gameroot;
    }
    
    
    public class Tiempo implements Runnable{
        

        @Override
        public void run() {
           
           while(morir ){   
                Platform.runLater(()->{     
                    tiempo.setText(Constantes.tiempostring(reloj));  
                    preguntarPoder();        
                    
                });
                
                try{
                    Thread.sleep(1000);  // significa la mitad de un segundo
                }catch (InterruptedException ex) {
                    System.out.println(MENSAJE);
                }
                
                reloj++;
            }
           
        }
        
    }
    
    /** Creación de los enemigos l 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
    public class Enemigos implements Runnable{
        private Insecto insecto;
        private int contador;
        public Enemigos(Insecto insecto){
          this.insecto = insecto;
          
      
        }
        
        
        

        @Override
        public void run() {
           
           while(morir && insecto.isHilo()){   
                Platform.runLater(()->{
                          
                    insecto.mover();// se esribiran los metodos que modifican la pantalla
                    insecto.consultar();
                });
                
                try{
                    Thread.sleep(250);  // significa la mitad de un segundo
                }catch (InterruptedException ex) {
                    System.out.println(MENSAJE);
                }
                //preguntarPoder()
                contador++;
            }
           
        }
        
    }
  /** Creación  de clase anonima 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
    
    public class Tiempovidas implements Runnable{
        private int contador;
        private int time;
        public Tiempovidas(int time){
            this.time= time;
        }
        
  /** Creación de los enemigos 
 *@param  vacio que usa el hilo de TIEMPO
 * @version 23/09/19
 */
        public void run() {
           disminuir= false;
            //System.out.println("EMPIEZA HILO TIEMPO")
           while(morir && contador<time){   
                Platform.runLater(()->{  
                    /**mientras siga vivo*/
                
                });
                
                try{
                    Thread.sleep(500);  // significa la mitad de un segundo
                }catch (InterruptedException ex) {
                    System.out.println(MENSAJE);
                }
                contador++;
            }
         disminuir= true;  
       
         
        }
        
    }
    
  /** Clase anónima poder que implementa runnable 
 *@param  vacio 
 * @version 23/09/19
 */
    
    public class Poder implements Runnable{
        
        private ImageView poderimg;
        public Poder(ImageView poder){
            this.poderimg = poder;
        }
        
        
        public void run() {
            //System.out.println("EMPIEZA HILO TIEMPO")
           while(morir && contadorA<=10){   // 5 segundos
              
               Platform.runLater(()->{   
                if (contadorA ==10 ){
                      gameroot.getChildren().remove(poder);
                     
                }
                
                });
                
                try{
                    Thread.sleep(500);  // significa la mitad de un segundo
                }catch (InterruptedException ex) {
                    System.out.println(MENSAJE);
                }
                contadorA++;
                
            }
           
        }
        
    }
      /** Clase anónima lagartij implementa runnable recuperarla despues de atraparla 
 *@param  vacio que usa el hilo de TIEMPO
 * @version 23/09/19
 */
     public class Lagartij implements Runnable{
        
        private int contador;
       
        @Override
        public void run() {
           
           while(morir && contador<=10){   
                Platform.runLater(()->{
                    if(contador==10){
                        lagartija.regresarImagen();
                        lagartija.setAtrapada(false);
                    }
                });
                
                try{
                    Thread.sleep(500);  // significa la mitad de un segundo
                }catch (InterruptedException ex) {
                    System.out.println(MENSAJE);
                }
                //preguntarPoder()
                contador++;
            }
           
        }
        
    }
}
