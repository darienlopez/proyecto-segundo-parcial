

package videogame;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import recursos.Constantes;
import static videogame.Videogame.panelcontrol;
import static videogame.Videogame.scene;

/** Creación del panel del Primer Nivel 
 *
 * @author Jsbarber
 * @version 23/09/19
 */

public class Table {
    private VBox root;
    private TableView tabla;
    private Button salir;
    
    public Table(){
        crearperfil();
    }
      /** Creación del Perfil
     * @param vacio que agrega los parametros que tendrá la scoreboard  
     * @version 23/09/19
     */
    public void crearperfil(){
        root = new VBox();
        salir = new Button("salir");
        root.setStyle("-fx-background-image: url('"+Constantes.RUTA+"/fondo_table.jpg');"
               + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+Constantes.ANCHO+" "+(Constantes.ALTO)+"; "
                + "-fx-background-position: center center;");
        HBox titulo = new HBox();
        titulo.setAlignment(Pos.CENTER);
        titulo.setStyle("-fx-background-color: grey;\n -fx-pading: 20; ");
        Font fuente = Font.font("veranda", FontWeight.BOLD,FontPosture.REGULAR, 18);
        Label labe = new Label("Historial de Jugadores");
        labe.setFont(fuente);
        labe.setTextFill(Color.WHITE);
        titulo.getChildren().add(labe);
        
        tabla = new TableView();
        TableColumn nombre = new TableColumn("Nombre");
        nombre.setMinWidth(100);
        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        TableColumn fecha = new TableColumn("Fecha");
        fecha.setMinWidth(100);
        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        
        TableColumn points = new TableColumn("Puntos");
        points.setMinWidth(100);
        points.setCellValueFactory(new PropertyValueFactory("puntos"));
        
        TableColumn level= new TableColumn("Nivel");
        level.setMinWidth(100);
        level.setCellValueFactory(new PropertyValueFactory<>("nivel"));
        tabla.getColumns().addAll(nombre,fecha,points,level);
        salir.setStyle("#round-red {\n" +
                        "    -fx-background-color: linear-gradient(#ff5400, #be1d00);\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 18px;\n"+
                        "    -fx-text-fill:  black;\n" +
                        "}");
        salir.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
               scene.setRoot(panelcontrol.getRoot());
            }
        });
        root.setAlignment(Pos.CENTER);
        tabla.getItems().addAll(PanelControl.partidas);
        root.getChildren().addAll(titulo,tabla,salir);
        
    }
    //getters y setters
    
    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }

    public TableView getTabla() {
        return tabla;
    }

    public void setTabla(TableView tabla) {
        this.tabla = tabla;
    }
    
}
