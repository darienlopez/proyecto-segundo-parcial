/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.util.Random;
import recursos.Constantes;


/** Creación del panel del Primer Nivel 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Hormiga extends Insecto{

    public Hormiga(){
        super("/mosca.png",45,45);
       
    }
    /** Se setea el movimiento de la hormiga 
     * @param vacio se setea el camino de las hormigas  
     * @version 23/09/19
     */
    
    @Override
    public void mover(){
        Random rnd = new Random();
        
        posicion = rnd.nextInt(2);
        if (posicion == 1){
            double x;
            if (ytop){
                x=enemigos.getLayoutX()-Constantes.DESPLAZAMIENTO; 
            }else{
                x=enemigos.getLayoutX()+Constantes.DESPLAZAMIENTO;
            }
            double y;
            if (ytop){
                y=enemigos.getLayoutY()-Constantes.DESPLAZAMIENTO; 
            }else{
                y=enemigos.getLayoutY()+Constantes.DESPLAZAMIENTO;
            }
           
              if (x>=0 && x<= Constantes.ALTO-50 && y>=0 && y<= Constantes.ALTO-50  )   {           
                enemigos.setLayoutY(y);
                enemigos.setLayoutX(x); 
            }
        }
        if (posicion == 0){
            double x;
            if (ytop){
                x=enemigos.getLayoutX()+Constantes.DESPLAZAMIENTO; 
            }else{
                x=enemigos.getLayoutX()-Constantes.DESPLAZAMIENTO;
            }
            double y;
            if (ytop){
                y=enemigos.getLayoutY()-Constantes.DESPLAZAMIENTO; 
            }else{
                y=enemigos.getLayoutY()+Constantes.DESPLAZAMIENTO;
            }
           
              if (x>=0 && x<= Constantes.ALTO-50 && y>=0 && y<= Constantes.ALTO-50  )   {           
                enemigos.setLayoutY(y);
                enemigos.setLayoutX(x); 
            }
        }
    }
}
