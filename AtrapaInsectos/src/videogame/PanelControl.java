/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import recursos.Constantes;
import static videogame.Videogame.scene;

/** Creación del panel principal de control 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class PanelControl {
    private VBox root;
    private Button iniciar;
    private Button ingresarP;
    private Button salir;
    private Table tabla;
    private GamePane panelJuego;
    protected static  ArrayList<Partida> partidas;
    protected static Thread hiloTiempo ;
    public static final String ESTILO = "#round-blue {\n" +
                        "    -fx-background-color: linear-gradient(#ff5400, #be1d00);\n" +
                        "    -fx-background-radius: 50;\n" +
                        "    -fx-background-insets: 0;\n" +
                        "    -fx-font-size: 18px;\n" +
                        "    -fx-text-fill:  black;\n" +
                        "}";
    
    
    /** Creación del constructor 
     * @param vacio lee la informacion de la clase data
     * @version 23/09/19
     */
    public PanelControl(){
       try {
           root = new VBox();
           partidas = new  ArrayList<>();
           root.setStyle("-fx-background-image: url('"+Constantes.RUTA+"/fondo_principal.jpg');"
                   + "-fx-background-repeat: stretch;"
                   + "-fx-background-size: "+Constantes.ANCHO+" "+Constantes.ALTO+"; "
                           + "-fx-background-position: center center;");
           
           /*Alert alert = new Alert(Alert.AlertType.ERROR)
           alert.setHeaderText(null)
           alert.setContentText("No se cargaron los ingredientes")
           alert.showAndWait()*/
           
           Data.leerInformacion();
           
           creacionBotones();   
           clickEnBotones();
       } catch (IOException ex) {
           Logger.getLogger(PanelControl.class.getName()).log(Level.SEVERE, null, ex);
       }
       
        
        
        
    }
    
    
    /** Creación de botones 
     * @param vacio se crea los botones que se van a mostrar en clase principal
     * @version 23/09/19
     */
    public void creacionBotones(){
        
        //inicializamos los botones
        iniciar = new Button("Iniciar");
        iniciar.setStyle(ESTILO);
        ingresarP = new Button("Tabla de puntajes");
        ingresarP.setStyle(ESTILO);
        salir = new Button("Salir");
        salir.setStyle(ESTILO);
        Media media = new Media(new File("src/recursos/Game_Intro.mp3").toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        MediaView mv = new MediaView(player);
        player.setAutoPlay(true);
        player.setVolume(1);
        player.setCycleCount(MediaPlayer.INDEFINITE);
        player.play();
        //aplicando estilos (cambiar luego por hojas de estilo)
        //agregar espacio entre el contenedor y los elementos internos
        root.setPadding(new Insets(10,10,10,10));
        
        //agregar espacio horizontal entre los elementos
        root.setSpacing(15);
        
        //alineacion de los botones al centro
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(iniciar, ingresarP, salir,mv);
        
    }
    /** Creación de evento al oprimir los botones
     * @param vacio se crea un evenHnadler para capturar las acciones 
     * @version 23/09/19
     */
    public void clickEnBotones(){
        iniciar.setOnAction(new EventHandler<ActionEvent>(){
            public void handle (ActionEvent event){
                try{    
                TextInputDialog dialog = new TextInputDialog(""); 
                dialog.setContentText("Ingrese nombre");     
                Optional<String> result = dialog.showAndWait();
                panelJuego = new GamePane(result.get());
                    scene.setRoot(panelJuego.getMainroot());
                }
                catch(NoSuchElementException ex) {
                    /**capturar excepcion */
                    
                }
                
                
                
                
            }
        });
        
        ingresarP.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
                System.out.println(partidas.size());
                tabla = new Table();
                scene.setRoot(tabla.getRoot());
            }
        });
        salir.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent evento){
               Platform.exit();
            }
        });
    }

    /** Creación del panel 
     * @return root para crear el panel 
     * @version 23/09/19
     */
    public VBox getRoot() {
        return root;
    } 
}
