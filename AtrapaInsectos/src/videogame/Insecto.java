/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videogame;

import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import recursos.Constantes;

/** Creación de la clase Padre insecto  
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Insecto {
    public Image img;
    public ImageView enemigos;
    private boolean hilo =true;
    public int actualizarmov =0 ;
    public int posicion;
    private Random x;
    private Random y;
    private double x1;
    private double y1;
    public boolean xtop;
    public boolean ytop;
    public Insecto(String url,int sizex,int sizey){
        img = new Image(getClass().getResourceAsStream(Constantes.RUTA+url),sizex,sizey,true,true);
        enemigos = new ImageView(img);
        crear();
        xtop = true;
        ytop = true;
    }
    /** Se crea el método crear para inicializar los insectos 
     * @param vacio y se setea el tamaño que ocupa la imagen  
     * @version 23/09/19
     */
    public void crear(){
        x = new Random();
            x1 = 50 + (Constantes.ANCHO-50 - 50) * x.nextDouble();
             y = new Random();
            y1 = 50 + (Constantes.ALTO-50 - 50) * y.nextDouble();
          getEnemigos().setLayoutX(x1);
            getEnemigos().setLayoutY(y1);
    }
    
    /** Creación de números aleatorios para path de los insectos
     * @param vacio  
     * @version 23/09/19
     */
    public void generarnum(){
           Random rnd = new Random();
        
        actualizarmov = rnd.nextInt(4);  
        posicion = actualizarmov;
    }
    
    /** Constructor vacio 
     * @param vacio  
     * @version 23/09/19
     */
    public void mover(){
        /** se escribiran los metodos que 
         modifican la pantalla
         */
        
    }
/** Get de la imagen de los insectos 
     * @param vacio  
     * @return img
     * @version 23/09/19
     */
    public Image getImg() {
        return img;
    }
/** Set de la imagen de los insectos 
     * @param Image  
     * @version 23/09/19
     */
    public void setImg(Image img) {
        this.img = img;
    }
/** Get de la imagen de los enemigos
     * @param vacio  
     * @return enemigos
     * @version 23/09/19
     */
    public ImageView getEnemigos() {
        return enemigos;
    }
/** Set de la imagen de los enemigos 
     * @param ImageView
     * @version 23/09/19
     */
    public void setEnemigos(ImageView enemigos) {
        this.enemigos = enemigos;
    }
/** Boolean si es un hilo 
     * @return boolean 
     * @version 23/09/19
     */
    public boolean isHilo() {
        return hilo;
    }
/** Set de los hilos 
     * @param boolean
     * @version 23/09/19
     */
    public void setHilo(boolean hilo) {
        this.hilo = hilo;
    }
    /** Setea las constantes de los enemigos  
     * @param Vacio
     * @version 23/09/19
     */
    public void consultar(){
        if (enemigos.getLayoutY()<=10){
            ytop = false;
        }
        if (enemigos.getLayoutY()>=Constantes.ALTO-70){
            ytop=true;
        }
        if (enemigos.getLayoutX()<=10){
            xtop = false;
        }
        if (enemigos.getLayoutX()>=Constantes.ANCHO-70){
            xtop=true;
        }
        
        
    }
}
