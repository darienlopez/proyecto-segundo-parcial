

package videogame;

import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import recursos.Constantes;


/** Creación del panel del Primer Nivel 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Telarana {
    protected static Image img;
    protected static ImageView tela;
    protected static int posicion;
    public  Telarana(){
        img = new Image(getClass().getResourceAsStream(Constantes.RUTA+"/tela.png"),50,50,true,true);
        tela = new ImageView(img);
        ubicar();
    }
     /** Creación del poder tele
     * @param vacio que dimensiona el tamaño de la tela 
     * @version 23/09/19
     */
    public void ubicar(){
        
        
    Random x = new Random();
            double x1 = 50 + (Constantes.ANCHO-50 - 50) * x.nextDouble();
            Random y = new Random();
            double y1 = 50 + (Constantes.ALTO-50 - 50) * y.nextDouble();
            
            tela.setLayoutX(x1);
            tela.setLayoutY(y1);
    }
}
