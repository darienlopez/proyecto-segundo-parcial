/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/** Constantes del juego para usarlas en las clases 
 *
 * @author Jsbarber
 * @version 23/09/19
 */
public class Constantes {
    public static  final String RUTA= "/recursos";
    public static final double ANCHO = 500;
    public static final double ALTO = 500;
    public static final double DESPLAZAMIENTO = 10;
    public static String tiempostring(int tiempo){
        TimeZone tz= TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat ("mm:ss");
        df.setTimeZone(tz);
        return df.format(new Date(tiempo *1000L));
    }
        private Constantes() {
    throw new IllegalStateException("");
  }
    }
    

